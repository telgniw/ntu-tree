//
//  NTRHistoryPageViewController.m
//  WoodFairy
//
//  Created by Huang Yi on 12/12/27.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import "NTRHistoryPageViewController.h"
#import "NTRHistoryPageContentViewController.h"
#import "NTRHistoryDetailViewController.h"

@interface NTRHistoryPageViewController ()

@end

@implementation NTRHistoryPageViewController

#pragma mark Object Lifecycle

- (id)initWithCoder:(NSCoder *)aDecoder {
    if(self = [super initWithCoder:aDecoder andNumberOfPages:2]) {
        storyController = nil;
        
        [self setDelegate:self];
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if(self.petName != nil && self.petClass != nil) {
        if([self.petClass isEqualToString:@"椰子"]) {
            [self.pageControl setCurrentPage:0];
            [self changePage:self.pageControl];
        }
        else {
            [self.pageControl setCurrentPage:1];
            [self changePage:self.pageControl];
        }
        
        if(storyController == nil) {
            storyController = [[NTRStoryEndingViewController alloc] initWithName:self.petName andClassName:self.petClass];
            [storyController setDelegate:self];
            [storyController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
            
            [self presentViewController:storyController animated:YES completion:nil];
        }
    }
}

#pragma mark -
#pragma mark NTRPageViewDelegate

- (UIViewController *)viewControllerForPage:(NSInteger)page {
    NSString *petClass = nil;
    switch(page) {
        case 0:
            petClass = @"椰子";
            break;
        case 1:
            petClass = @"楓香";
            break;
        default:
            break;
    }
    return [[NTRHistoryPageContentViewController alloc] initWithPetClass:petClass andSuperViewController:self];
}

#pragma mark -
#pragma mark NTRFullPagePromptDelegate

- (void)handleFullPagePromptButton:(id)sender {
    [sender dismissViewControllerAnimated:YES completion:^{
        storyController = nil;
        
        NTRHistoryDetailViewController *controller = [[NTRHistoryDetailViewController alloc] initWithClass:self.petClass];
        [self.navigationController pushViewController:controller animated:YES];
        
        self.petName = nil;
        self.petClass = nil;
    }];
}

- (void)handleFullPagePromptDismiss:(id)sender {
    // do nothing
}

@end
