//
//  NTRUtility.m
//  NTU-Tree-iOS
//
//  Created by Huang Yi on 12/12/12.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//
//  Reference http://stackoverflow.com/a/5410443/881930
//

#import "NTRUtility.h"

#pragma mark UIImage Resize

@implementation UIImage (NTRResize)

+ (UIImage *)imageNamed:(NSString *)name withSize:(CGSize)size {
    return [[UIImage imageNamed:name] resize:size withQuality:kCGInterpolationDefault];
}

- (UIImage *)resize:(CGSize)newSize {
    return [self resize:newSize withQuality:kCGInterpolationDefault];
}

- (UIImage *)resize:(CGSize)newSize withQuality:(CGInterpolationQuality)quality {
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    
    CGImageRef imageRef = self.CGImage;
    CGContextRef bitmap = CGBitmapContextCreate(nil, newRect.size.width, newRect.size.height, CGImageGetBitsPerComponent(imageRef), 0, CGImageGetColorSpace(imageRef), CGImageGetBitmapInfo(imageRef));
    
    if(bitmap == nil) {
        NSLog(@"Warning: `CGContextRef bitmap` is nil.");
        return nil;
    }
    
    CGContextSetInterpolationQuality(bitmap, quality);
    CGContextDrawImage(bitmap, newRect, imageRef);
    
    CGImageRef newImageRef = CGBitmapContextCreateImage(bitmap);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef scale:self.scale orientation:self.imageOrientation];
    
    CGContextRelease(bitmap);
    CGImageRelease(newImageRef);
    
    return newImage;
}

@end

#pragma mark -
#pragma mark UIView Animation

@implementation UIView (NTRAnimation)

- (void)fadeInWithDuration:(NSTimeInterval)duration {
    [self fadeTo:1.0f withDuration:duration completion:nil];
}

- (void)fadeInWithDuration:(NSTimeInterval)duration completion:(void (^)(BOOL finished))completion {
    [self fadeTo:1.0f withDuration:duration completion:completion];
}

- (void)fadeOutWithDuration:(NSTimeInterval)duration {
    [self fadeTo:0.0f withDuration:duration completion:nil];
}

- (void)fadeOutWithDuration:(NSTimeInterval)duration completion:(void (^)(BOOL finished))completion {
    [self fadeTo:0.0f withDuration:duration completion:completion];
}

- (void)fadeTo:(CGFloat)alpha withDuration:(NSTimeInterval)duration completion:(void (^)(BOOL finished))completion {
    [UIView animateWithDuration:duration animations:^(void) {
        [self setAlpha:alpha];
    } completion:completion];
}

@end

#pragma mark -
#pragma mark NSURL Parse

@implementation NSURL (NTRParse)

- (NSString *)valueWithKey:(NSString *)key {
    NSArray *params = [[self query] componentsSeparatedByString:@"&"];
    for(NSString *param in params) {
        NSArray *pair = [param componentsSeparatedByString:@"="];
        if([pair count] < 2) {
            continue;
        }
        
        if([key isEqualToString:[(NSString *)pair[0] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]) {
            NSString *value = [(NSString *)pair[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            return value;
        }
    }
    return nil;
}

@end
