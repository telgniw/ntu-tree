//
//  NTRHistoryDetailViewController.m
//  NTU-Tree-iOS
//
//  Created by Huang Yi on 12/12/11.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import "NTRHistoryDetailViewController.h"
#import "NTRUtility.h"
#import "NTRDataSource.h"

static NSString *cellIdentifier = @"HistoryCell";

#pragma mark HistoryData

@interface NTRHistoryData ()

@end

@implementation NTRHistoryData

- (id)initWithDate:(NSDate *)date andImage:(UIImage *)image {
    if(self = [super init]) {
        _date = date;
        _image = image;
    }
    return self;
}

@end

#pragma mark -
#pragma mark HistoryTableViewCell

@interface NTRHistoryTableViewCell ()

@end

@implementation NTRHistoryTableViewCell

@end

#pragma mark -
#pragma mark HistoryDetailViewController

@interface NTRHistoryDetailViewController ()

@end

@implementation NTRHistoryDetailViewController

- (id)initWithClass:(NSString *)className {
    if(self = [super initWithNibName:@"NTRHistoryDetailViewController" bundle:nil]) {
        petClass = className;
        
        NSDictionary *history = [[NTRDataSource sharedDataSource] historyForClass:className];
        NSMutableArray *objects = [NSMutableArray array];
        for(NSDictionary *data in history[NTRDataKeyRecord]) {
            if([data[NTRDataKeyTime] integerValue] < 0) {
                continue;
            }
            
            NSDate *date = [NSDate dateWithTimeIntervalSinceReferenceDate:[data[NTRDataKeyTime] doubleValue]];
            UIImage *image = [UIImage imageNamed:[[NTRDataSource sharedDataSource] imageNameForClass:className andStage:data[NTRDataKeyStage]]];
            NTRHistoryData *object = [[NTRHistoryData alloc] initWithDate:date andImage:image];
            [objects addObject:object];
        }
        
        _data = [objects sortedArrayUsingComparator:^NSComparisonResult(NTRHistoryData *obj1, NTRHistoryData *obj2) {
            return [obj2.date compare:obj1.date];
        }];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundHistory02.png" withSize:self.view.frame.size]];
    
    [self.navigationItem setTitle:petClass];
}

#pragma mark DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NTRHistoryTableViewCell *dataCell = nil;
    
    if([cell isKindOfClass:[NTRHistoryTableViewCell class]]) {
        dataCell = (NTRHistoryTableViewCell *)cell;
    }
    else {
        NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"NTRHistoryTableViewCell" owner:self options:nil];
        
        for(id object in objects) {
            if([object isKindOfClass:[NTRHistoryTableViewCell class]]) {
                dataCell = (NTRHistoryTableViewCell *)object;
                break;
            }
        }
    }
    
    if(dataCell != nil) {
        NTRHistoryData *data = self.data[indexPath.row];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM/dd HH:mm"];
        
        [dataCell.dateLabel setFont:[UIFont fontWithName:@"DFGirlStd-W5" size:15.0f]];
        dataCell.dateLabel.text = [formatter stringFromDate:data.date];
        dataCell.petLabel.image = data.image;
    }
    
    return dataCell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if([self tableView:self.tableView numberOfRowsInSection:section] > 1) {
        return nil;
    }
    
    if(footerView == nil) {
        footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, [self tableView:self.tableView heightForFooterInSection:section])];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.f, 320.f)];
        imageView.image = [UIImage imageNamed:@"Timeline-iPhone.png"];
        
        [footerView addSubview:imageView];
    }
    
    return footerView;
}

#pragma mark Delegatge

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // prevent cell from overlapping
    return 320.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if([self tableView:self.tableView numberOfRowsInSection:section] > 1) {
        return 0.0f;
    }
    
    // prevent empty tails
    return self.tableView.frame.size.height - 320.0f;
}

#pragma mark IBAction

- (void)handleTap:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
