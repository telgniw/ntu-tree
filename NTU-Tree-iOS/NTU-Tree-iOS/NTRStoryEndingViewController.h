//
//  NTRStoryEndingViewController.h
//  WoodFairy
//
//  Created by Huang Yi on 12/12/26.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NTRFullPagePromptController.h"

@interface NTRStoryEndingViewController : NTRFullPagePromptController {
    NSString *petClass;
}

@property (strong, nonatomic) IBOutlet UIImageView *imageView;

- (id)initWithName:(NSString *)name andClassName:(NSString *)className;

@end
