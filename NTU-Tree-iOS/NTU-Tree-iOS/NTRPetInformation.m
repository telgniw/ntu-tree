//
//  NTRPetInfo.m
//  NTU-Tree-iOS
//
//  Created by Huang Yi on 12/12/16.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import "NTRPetInformation.h"
#import "NTRDataSource.h"


const NSInteger NTRMaxExperience = 100;

static NSString *NTRPetKeyFirstUsed = @"NTRPetInformation.UserDefaults.FirstUsed";
static NSString *NTRPetKeyName      = @"NTRPetInformation.UserDefaults.Name";
static NSString *NTRPetKeyClassName = @"NTRPetInformation.UserDefaults.ClassName";
static NSString *NTRPetKeyStage     = @"NTRPetInformation.UserDefaults.Stage";
static NSString *NTRPetKeyMood      = @"NTRPetInformation.UserDefaults.Mood";
static NSString *NTRPetKeyExp       = @"NTRPetInformation.UserDefaults.Exp";

@implementation NTRPetInformation

+ (BOOL)firstUsed {
    return [[NSUserDefaults standardUserDefaults] objectForKey:NTRPetKeyFirstUsed] == nil;
}

+ (NSString *)className {
    return [[NSUserDefaults standardUserDefaults] stringForKey:NTRPetKeyClassName];
}

+ (NSString *)imageName {
    return [[NTRDataSource sharedDataSource] imageNameForClass:[self className] andStage:[self stage]];
}

+ (NSString *)name {
    return [[NSUserDefaults standardUserDefaults] stringForKey:NTRPetKeyName];
}

+ (NSString *)stage {
    return [[NSUserDefaults standardUserDefaults] stringForKey:NTRPetKeyStage];
}

+ (NTRPetMood)mood {
    return [[NSUserDefaults standardUserDefaults] integerForKey:NTRPetKeyMood];
}

+ (NSInteger)experience {
    return [[NSUserDefaults standardUserDefaults] integerForKey:NTRPetKeyExp];
}

+ (void)initWithNewName:(NSString *)name forClass:(NSString *)className {
    NTRDataSource *dataSource = [NTRDataSource sharedDataSource];
    [dataSource updateToCurrentTimeByClass:className];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"" forKey:NTRPetKeyFirstUsed];
    [defaults setObject:name forKey:NTRPetKeyName];
    [defaults setObject:className forKey:NTRPetKeyClassName];
    [defaults setObject:[dataSource firstStageForClass:className] forKey:NTRPetKeyStage];
    [defaults setInteger:NTRMoodUnhappy forKey:NTRPetKeyMood];
    [defaults setInteger:0 forKey:NTRPetKeyExp];
}

+ (void)reset {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:NTRPetKeyName];
    [defaults removeObjectForKey:NTRPetKeyClassName];
    [defaults removeObjectForKey:NTRPetKeyStage];
    [defaults removeObjectForKey:NTRPetKeyMood];
    [defaults removeObjectForKey:NTRPetKeyExp];
}

+ (void)resetAll {
    [self reset];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:NTRPetKeyFirstUsed];
    
    NTRDataSource *dataSource = [NTRDataSource sharedDataSource];
    [dataSource updateAllToScanned:NO];
    [dataSource updateAllToNoTime];
}

+ (NTRLevelUpStatus)increaseExperienceWithGain:(NTRPetExperienceGain)gain {
    NTRLevelUpStatus hasLeveledUp = NTRLevelUpNone;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSInteger exp = [defaults integerForKey:NTRPetKeyExp];
    switch(gain) {
        case NTRExperienceSmallGain:
            exp += arc4random() % 10 + 10;
            break;
        case NTRExperienceMedianGain:
            exp += arc4random() % 10 + 20;
            break;
        case NTRExperienceSuperGain:
            exp += arc4random() % 20 + 60;
            break;
        default:
            exp += 100;
            break;
    }
    
    if(exp >= NTRMaxExperience) {
        hasLeveledUp = NTRLevelUpMiddle;
        exp = 0;
        
        NTRDataSource *dataSource = [NTRDataSource sharedDataSource];
        NSString *className = [self className];
        NSString *nextStage = [dataSource nextStageForClass:className afterStage:[self stage]];
        // update leveled-up time
        [dataSource updateToCurrentTimeByClass:className andStage:nextStage];
        if([dataSource nextStageForClass:className afterStage:nextStage] != nil) {
            // update current stage
            [defaults setObject:nextStage forKey:NTRPetKeyStage];
        }
        else {
            // the pet has reached the maximun level, reset the game
            hasLeveledUp = NTRLevelUpMax;
            [self reset];
        }
    }
    
    [defaults setInteger:NTRMoodHappy forKey:NTRPetKeyMood];
    [defaults setInteger:exp forKey:NTRPetKeyExp];
    return hasLeveledUp;
}

@end
