//
//  NTRAppDelegate.m
//  NTU-Tree-iOS
//
//  Created by Huang Yi on 12/11/16.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import "NTRAppDelegate.h"
#import "NTRDataSource.h"

@implementation NTRAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // force view class to load so it may be referenced directly from NIB
    [ZBarReaderView class];
    
    // custom navigation bar
    [[UINavigationBar appearance] setBackgroundColor:[UIColor colorWithRed:0.52f green:0.26f blue:0.0f alpha:1.0f]];
    [[UINavigationBar appearance] setTintColor:[UIColor brownColor]];

    return YES;
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    [[NTRDataSource sharedDataSource] cleanCache];
}

@end
