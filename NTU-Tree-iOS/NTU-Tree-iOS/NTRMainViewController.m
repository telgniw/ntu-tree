//
//  NTRMainViewController.m
//  NTU-Tree-iOS
//
//  Created by Huang Yi on 12/12/9.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import "NTRMainViewController.h"
#import "NTRTabBarViewController.h"
#import "NTRUtility.h"

#import "NTRPetInformation.h"

@interface NTRMainViewController ()

@end

@implementation NTRMainViewController

#pragma mark -
#pragma mark Object Lifecycle

- (id)initWithCoder:(NSCoder *)aDecoder {
    if(self = [super initWithCoder:aDecoder]) {
        storyController = nil;
        
        updateNeeded = NO;
        dialogSeq = 0;
        
        pokeCount = 0;
        previousMessage = -1;
        messages = [NSArray arrayWithObjects:@"我餓了！餵我吃東西！", @"嗨～", @"主人我最喜歡你了❤", @"好想要快快長大呀！", @"（～￣▽￣～）海帶啊～海帶～", @"~(￣▽￣)~(＿△＿)~(￣▽￣)~", @"╰（‵□′）╯快餵我吃東西！", @"o(〒﹏〒)o肚子好餓喔～", @"＜( ￣︿￣)︵θ︵θ︵☆流星拳攻擊！", @"\\(\"▔□▔)/ 舉手投降", @"不要戳啦！煩耶(＞﹏＜)", @"呼呼呼(￣o￣) . z Z", nil];
        
        // restart animation after application brought to foreground from background
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startAnimation) name:UIApplicationWillEnterForegroundNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
}

#pragma mark -
#pragma mark View Display

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // initialize display
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundMain.png" withSize:self.view.frame.size]];
    
    [self.dialogView setAlpha:0.0f];
    [self.dialogLabel setAlpha:0.0f];
    [self.dialogLabel setFont:[UIFont fontWithName:@"DFGirlStd-W5" size:15.0f]];
    
    [self.nameLabel setFont:[UIFont fontWithName:@"DFGirlStd-W5" size:17.0f]];
    [self.progressLabel setFont:[UIFont fontWithName:@"DFGirlStd-W5" size:15.0f]];
    [self.tapView requireGestureRecognizerToFail:self.doubleTapView];
    
    updateNeeded = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if(updateNeeded) {
        [self forceUpdateView];
        updateNeeded = NO;
    }
    
    [self startAnimation];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // no pet
    if([NTRPetInformation name] == nil) {
        if([NTRPetInformation firstUsed]) {
            storyController = [[NTRFullPagePromptController alloc] initWithText:@"預言家：報告國王，根據星象顯示國家將面臨一場災難，需要神奇的樹木才能夠拯救世界！\n\n國王：什麼，終於到這時候了嗎？快把我們國家珍藏已久的種子拿出來，並且召喚那位傳說中的園丁吧……"];
            [storyController setBackgroundImage:[UIImage imageNamed:@"backgroundStory.png"]];
            [storyController setDelegate:self];
            [storyController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
            
            [storyController.textLabel setTextColor:[UIColor colorWithRed:1.0f green:1.0f blue:0.4f alpha:1.0f]];
            [storyController.button setTitle:@"點此選種子拯救世界" forState:UIControlStateNormal];
            
            [self presentViewController:storyController animated:YES completion:nil];
        }
        else {
            NTRSelectionViewController *controller = [[NTRSelectionViewController alloc] init];
            [controller setDelegate:self];
            [controller setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
            
            [self presentViewController:controller animated:YES completion:nil];
        }
    }
}

- (void)startAnimation {
    // start with initial state for the animation to work
    self.imageView.transform = CGAffineTransformIdentity;
    
    [UIView animateWithDuration:0.5f delay:0.1f options: UIViewAnimationOptionAllowUserInteraction|UIViewAnimationOptionAutoreverse|UIViewAnimationOptionRepeat animations:^(void){
        self.imageView.transform = CGAffineTransformMakeTranslation(0.0f, 5.0f);
    } completion:nil];
}

#pragma mark -
#pragma mark NTRFullPagePromptDelegate

- (void)handleFullPagePromptDismiss:(id)sender {
    [self setUpdateNeeded];
}

- (void)handleFullPagePromptButton:(id)sender {
    NTRSelectionViewController *controller = [[NTRSelectionViewController alloc] init];
    [controller setDelegate:self];
    [controller setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    
    [sender presentViewController:controller animated:YES completion:nil];
}

#pragma mark -
#pragma mark NTRSelectionViewDelegate

- (void)handleSelection:(id)sender {
    [self setUpdateNeeded];
    
    if(storyController != nil) {
        [storyController setShouldDismissAfterRestart:YES];
        storyController = nil;
    }
    
    [sender dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark Others

- (void)setUpdateNeeded {
    updateNeeded = YES;
}

- (void)showDialog:(NSString *)message {
    [self.dialogView fadeInWithDuration:0.5f];
    [self.dialogLabel setText:message];
    [self.dialogLabel fadeInWithDuration:0.5f];
    
    dialogSeq++;
    [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(hideDialog:) userInfo:[NSNumber numberWithInt:dialogSeq] repeats:NO];
}

- (void)hideDialog {
    [self.dialogView fadeOutWithDuration:0.5f];
    [self.dialogLabel fadeOutWithDuration:0.5f];
}

- (void)hideDialog:(NSTimer *)timer {
    if([timer.userInfo compare:[NSNumber numberWithInt:dialogSeq]] == NSOrderedSame) {
        [self hideDialog];
    }
}

- (IBAction)handleTap:(id)sender {
    if(sender == self.tapView) {
        int currentMessage;
        do {
            currentMessage = arc4random() % [messages count];
        }
        while(currentMessage == previousMessage && [messages count] > 1);
        
        [self hideDialog];
        [self showDialog:messages[currentMessage]];
        previousMessage = currentMessage;
    }
    else if(sender == self.doubleTapView){
        [self hideDialog];
        
        self.imageView.transform = CGAffineTransformIdentity;
        [UIView animateWithDuration:1.0f delay:0.0f options: UIViewAnimationOptionAllowAnimatedContent animations:^(void){
            self.imageView.transform = CGAffineTransformMakeScale(0.5, 0.5);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:1.0f delay:1.0f options: UIViewAnimationOptionAllowAnimatedContent|UIViewAnimationOptionCurveEaseInOut animations:^(void){
                self.imageView.transform = CGAffineTransformMakeScale(1.0, 1.0);
            } completion:^(BOOL finished) {
                [self startAnimation];
            }];
        }];
    }
}

- (void)forceUpdateView {
    if([NTRPetInformation name] != nil) {
        // initialize information shown on screen
        self.nameLabel.text = [NTRPetInformation name];
        
        int exp = [NTRPetInformation experience];
        self.progressLabel.text = [NSString stringWithFormat:@"%d%%", exp];
        self.progressView.progress = (double)exp / (double)NTRMaxExperience;
        
        self.imageView.image = [UIImage imageNamed:[NTRPetInformation imageName]];
    }
}

// Demo Feature
- (IBAction)handleSwipe:(id)sender {
    NSString *petName = [NTRPetInformation name];
    NSString *className = [NTRPetInformation className];
    
    BOOL hasLeveledUp = [NTRPetInformation increaseExperienceWithGain:NTRExperienceDemoGain];
    
    NTRTabBarViewController *tabBarController = (NTRTabBarViewController *)self.tabBarController;
    [tabBarController handleLevelUp:hasLeveledUp forPet:petName andPetClass:className withDelegate:self];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    [self forceUpdateView];
    [self startAnimation];
}

@end
