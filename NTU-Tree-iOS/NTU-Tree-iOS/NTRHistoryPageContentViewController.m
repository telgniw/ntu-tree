//
//  NTRHistoryPageContentViewController.m
//  WoodFairy
//
//  Created by Huang Yi on 12/12/27.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import "NTRHistoryPageContentViewController.h"
#import "NTRHistoryDetailViewController.h"
#import "NTRDataSource.h"
#import "NTRUtility.h"

@interface NTRHistoryPageContentViewController ()

@end

@implementation NTRHistoryPageContentViewController

- (id)initWithPetClass:(NSString *)className andSuperViewController:(UIViewController *)controller {
    if(self = [super initWithNibName:@"NTRHistoryPageContentViewController" bundle:nil]) {
        petClass = className;
        
        _superViewController = controller;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundHistoryNew.png" withSize:self.view.frame.size]]];
    
    [self.imageView setImage:[UIImage imageNamed:[[NTRDataSource sharedDataSource] imageNameForClass:petClass]]];
    [self.textLabel setText:petClass];
    [self.textLabel setFont:[UIFont fontWithName:@"DFGirlStd-W5" size:self.textLabel.font.pointSize]];
}

- (IBAction)handleTap:(id)sender {
    if([[NTRDataSource sharedDataSource] lastStageForClass:petClass] == nil) {
        [[[UIAlertView alloc] initWithTitle:@"沒有歷史"
                                    message:@"你還沒有養過這棵樹唷！趕快去養！"
                                   delegate:nil
                          cancelButtonTitle:@"我知道了"
                          otherButtonTitles:nil] show];
    }
    else {
        NTRHistoryDetailViewController *controller = [[NTRHistoryDetailViewController alloc] initWithClass:petClass];
        [self.superViewController.navigationController pushViewController:controller animated:YES];
    }
}

@end
