//
//  NTRHistoryPageContentViewController.h
//  WoodFairy
//
//  Created by Huang Yi on 12/12/27.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NTRHistoryPageContentViewController : UIViewController {
    NSString *petClass;
}

@property (nonatomic) UIViewController *superViewController;

@property (strong, nonatomic) IBOutlet UIImageView *frameView;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *textLabel;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGesture;

- (id)initWithPetClass:(NSString *)className andSuperViewController:(UIViewController *)controller;

- (IBAction)handleTap:(id)sender;

@end
