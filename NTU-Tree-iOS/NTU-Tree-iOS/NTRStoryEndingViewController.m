//
//  NTRStoryEndingViewController.m
//  WoodFairy
//
//  Created by Huang Yi on 12/12/26.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "NTRStoryEndingViewController.h"
#import "NTRDataSource.h"
#import "NTRUtility.h"

@interface NTRStoryEndingViewController ()

@end

@implementation NTRStoryEndingViewController

- (id)initWithName:(NSString *)name andClassName:(NSString *)className {
    if(self = [super initWithNibName:@"NTRStoryEndingViewController" andText:[NSString stringWithFormat:@"你的%@已經順利長大了，踏上拯救世界的旅程。它的偉大故事將永遠記錄在歷史之中。請你繼續努力，培養出更多的小樹吧！", name]]) {
        
        petClass = className;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setBackgroundImage:[UIImage imageNamed:@"backgroundGo.png"]];
    
    NTRDataSource *dataSource = [NTRDataSource sharedDataSource];
    [self.imageView setImage:[UIImage imageNamed:[dataSource imageNameForClass:petClass]]];
}

@end
