//
//  NTRQRcodeViewController.m
//  NTU-Tree-iOS
//
//  Created by Chun-Ta, Lin on 12/12/10.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import "NTRQRcodeViewController.h"
#import "NTRTabBarViewController.h"
#import "NTRUtility.h"

#import "NTRDataSource.h"
#import "NTRPetInformation.h"

// Demo Feature Import
#import "NTRPageController.h"

@interface NTRQRcodeViewController ()

@end

@implementation NTRQRcodeViewController

@synthesize readerView;

- (void)viewDidLoad {
    [super viewDidLoad];
    readerView.readerDelegate = self;
    // you can use this to support the simulator
    if(TARGET_IPHONE_SIMULATOR) {
        cameraSim = [[ZBarCameraSimulator alloc]
                     initWithViewController: self];
        cameraSim.readerView = readerView;
    }
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundQRcode.png" withSize:self.view.frame.size]];
    
    [self.tipsLabel setText:@"掃描QRcode　\n讓小樹長大吧！"];
    [self.tipsLabel setFont:[UIFont fontWithName:@"DFGirlStd-W5" size:28.0f]];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if([NTRPetInformation name] == nil) {
        [[[UIAlertView alloc] initWithTitle:@"尚未選擇種子"
                                    message:@"你現在沒有小樹唷！先去選一棵種子吧！"
                                   delegate:self
                          cancelButtonTitle:nil
                          otherButtonTitles:@"馬上去", nil] show];
    }
    else {
        // run the reader when the view is visible
        [readerView start];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [readerView stop];
}

#pragma mark -
#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // Demo Feature
    if(alertView == self.alertResetAll || alertView == self.alertScannedTrees) {
        if(alertView == self.alertResetAll) {
            NTRTabBarViewController *tabBarController = (NTRTabBarViewController *)self.tabBarController;
            [tabBarController resetAll];
            [NTRPetInformation resetAll];
            [self.tabBarController setSelectedIndex:1];
        }
        else {
            [[NTRDataSource sharedDataSource] updateAllToScanned:YES];
            [self.tabBarController setSelectedIndex:2];
        }
        
        UINavigationController *controller = self.tabBarController.childViewControllers[2];
        NTRPageController *pageController = (NTRPageController *)controller.viewControllers[0];
        for(int i = 0; i < [pageController.tableViewControllers count]; i++) {
            NTRPageTableViewController *tableViewController = (NTRPageTableViewController *)pageController.tableViewControllers[i];
            if((NSNull *)tableViewController != [NSNull null]) {
                [tableViewController.view removeFromSuperview];
                [pageController.tableViewControllers replaceObjectAtIndex:i withObject:[NSNull null]];
            }
        }
        
        [pageController.pageControl setCurrentPage:0];
        [pageController changePage:self];
    }
    // Normal Scenario
    else {
        [self.tabBarController setSelectedIndex:1];
        
    }
}

#pragma mark -
#pragma mark ZBarReaderViewDelegate

- (void)readerView:(ZBarReaderView*)view
     didReadSymbols:(ZBarSymbolSet*)syms
          fromImage:(UIImage*)img {
    
    for(ZBarSymbol *sym in syms) {
        NSString *petName = [NTRPetInformation name];
        
        NSString *treeId = [[NSURL URLWithString:sym.data] valueWithKey:@"q"];
        if(treeId == nil) {
            [[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@不吃喔", petName]
                                        message:[NSString stringWithFormat:@"這是什麼？能吃嗎？你怎麼忍心餵\n你親愛的%@吃這種東西呢QQ", petName]
                                       delegate:nil
                              cancelButtonTitle:@"關閉"
                              otherButtonTitles: nil] show];
            return;
        }
        
        NSDictionary *treeData = [[NTRDataSource sharedDataSource] treeByID:treeId];
        if(treeData == nil) {
            [[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@挑食了", petName]
                                        message:[NSString stringWithFormat:@"對不起，%@不喜歡這棵樹耶QQ\n請換別棵樹試試看吧～", petName]
                                       delegate:nil
                              cancelButtonTitle:@"關閉"
                              otherButtonTitles:nil] show];
            
            return;
        }
        
        if([self.tabBarController isKindOfClass:[NTRTabBarViewController class]]) {
            NTRTabBarViewController *tabBarController = (NTRTabBarViewController *)self.tabBarController;
            [tabBarController updateWithTreeId:treeId];
        }
    }
}

#pragma mark -
#pragma mark Demo Feature: IBActions

- (IBAction)handleTap:(id)sender {
    if(sender == self.tapLeft) {
        if(self.alertResetAll == nil) {
            self.alertResetAll = [[UIAlertView alloc] initWithTitle:@"Demo快速鍵"
                                                            message:@"按繼續重設遊戲。"
                                                           delegate:self
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"繼續", nil];
        }
        [self.alertResetAll show];
    }
    else if(sender == self.tapRight) {
        if(self.alertScannedTrees == nil) {
            self.alertScannedTrees = [[UIAlertView alloc] initWithTitle:@"Demo快速鍵"
                                                                message:@"按繼續開啟所有圖鑑。"
                                                               delegate:self
                                                      cancelButtonTitle:nil
                                                      otherButtonTitles:@"繼續", nil];
        }
        [self.alertScannedTrees show];
    }
}

@end
