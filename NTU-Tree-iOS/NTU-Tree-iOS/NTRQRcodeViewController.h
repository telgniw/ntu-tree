//
//  NTRQRcodeViewController.h
//  NTU-Tree-iOS
//
//  Created by Chun-Ta, Lin on 12/12/10.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import "ZBarSDK.h"
#import <UIKit/UIKit.h>

@interface NTRQRcodeViewController : UIViewController<UIAlertViewDelegate, ZBarReaderViewDelegate> {
    ZBarReaderView *readerView;
    ZBarCameraSimulator *cameraSim;
}

@property (strong, nonatomic) IBOutlet ZBarReaderView *readerView;
@property (strong, nonatomic) IBOutlet UILabel *tipsLabel;

#pragma mark -
#pragma mark Demo Feature: IBOutlet and IBActions
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapLeft;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapRight;

@property (strong, nonatomic) UIAlertView *alertResetAll;
@property (strong, nonatomic) UIAlertView *alertScannedTrees;

- (IBAction)handleTap:(id)sender;

@end
