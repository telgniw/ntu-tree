//
//  main.m
//  NTU-Tree-iOS
//
//  Created by Huang Yi on 12/11/16.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NTRAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NTRAppDelegate class]));
    }
}
