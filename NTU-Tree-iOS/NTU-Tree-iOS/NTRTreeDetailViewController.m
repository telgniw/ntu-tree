//
//  NTRTreeDetailViewController.m
//  NTU-Tree-iOS
//
//  Created by Huang Yi on 12/12/11.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import "NTRTreeDetailViewController.h"
#import "NTRDataSource.h"
#import "NTRUtility.h"

@interface NTRTreeDetailViewController ()

@property (strong, nonatomic, readwrite) NSString *name;

@end

@implementation NTRTreeDetailViewController

- (id)initWithName:(NSString *)name {
    if(self = [super initWithNibName:@"NTRTreeDetailViewController" bundle:nil]) {
        _name = name;
    }
    return self;
}

- (id)initWithName:(NSString *)name andRegion:(NSString *)region {
    if(self = [super initWithNibName:@"NTRTreeDetailViewController" bundle:nil]) {
        _name = name;
        _region = region;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSDictionary *tree = [[NTRDataSource sharedDataSource] treeByName:self.name andRegion:self.region];
    if(tree == nil)
        tree = [[NTRDataSource sharedDataSource] treeByName:self.name];
    
    self.nameLabel.text = self.name;
    self.binomialLabel.text = tree[NTRDataKeyBinomial];
    self.classLabel.text = tree[NTRDataKeyClass];
    self.storyLabel.text = tree[NTRDataKeyStory];
    self.imageView.image = [UIImage imageNamed:tree[NTRDataKeyImage] withSize:self.imageView.frame.size];
    
    [self.storyLabel sizeToFit];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.scrollView setContentOffset:CGPointZero];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // scroll to the bottom of view
    [self.scrollView setContentSize:CGSizeMake(320.0f, 455.0f)];
    
    CGPoint scrollOffset = CGPointMake(0.0f, self.scrollView.contentSize.height - self.scrollView.frame.size.height);
    if(scrollOffset.y < 0.0f) {
        scrollOffset.y = 0.0f;
    }
    
    [self.scrollView setContentOffset:scrollOffset animated:YES];
}

@end
