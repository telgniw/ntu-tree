//
//  NTRDataSource.h
//  NTU-Tree-iOS
//
//  Created by Chun-Ta, Lin on 12/12/14.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const NTRTrue;

extern NSString * const NTRDataKeyID;
extern NSString * const NTRDataKeyName;
extern NSString * const NTRDataKeyRegion;
extern NSString * const NTRDataKeyBinomial;
extern NSString * const NTRDataKeyImage;
extern NSString * const NTRDataKeyStory;
extern NSString * const NTRDataKeyScanned;
extern NSString * const NTRDataKeyClass;
extern NSString * const NTRDataKeyTime;
extern NSString * const NTRDataKeyStage;
extern NSString * const NTRDataKeyRecord;

@interface NTRDataSource : NSObject {
    // Main data pool
    NSArray *treeList;
    NSArray *historyList;
    
    // Cache data pool
    NSCache *cache;

    // Data path
    NSString *treePath;
    NSString *historyPath;
}

+ (NTRDataSource *)sharedDataSource;

- (void)refresh;
- (void)cleanCache;

#pragma mark trees.plist

- (NSDictionary *)treeByID:(NSString *)treeId;
- (NSDictionary *)treeByName:(NSString *)name;
- (NSDictionary *)treeByName:(NSString *)name andRegion:(NSString *)region;
- (NSArray *)arrayWithRegions;
- (NSArray *)arrayWithTreesInRegion:(NSString *)region;
- (NSArray *)arrayWithGroupedTreesInRegion:(NSString *)region;
- (NSInteger)countOfTreesInRegion:(NSString *)region;
- (NSInteger)countOfScannedTreesInRegion:(NSString *)region;
- (NSInteger)indexOfRegion:(NSString *)region;
- (NSInteger)indexOfTree:(NSString *)name inRegion:(NSString *)region;
- (BOOL)updateToScannedByTreeId:(NSString *)treeId;
- (BOOL)updateToScanned:(BOOL)scanned byTreeId:(NSString *)treeId;
- (BOOL)updateAllToScanned:(BOOL)scanned;

#pragma mark -
#pragma mark history.plist

- (NSDictionary *)historyRecordsForClass:(NSString *)className andStage:(NSString *)stage;
- (NSDictionary *)historyForClass:(NSString *)className;
- (NSString *)imageNameForClass:(NSString *)className;
- (NSString *)imageNameForClass:(NSString *)className andStage:(NSString *)stage;
- (NSTimeInterval)timeForClass:(NSString *)className andStage:(NSString *)stage;
- (NSString *)firstStageForClass:(NSString *)className;
- (NSString *)lastStageForClass:(NSString *)className;
- (NSString *)nextStageForClass:(NSString *)className afterStage:(NSString *)stage;
- (BOOL)updateToCurrentTimeByClass:(NSString *)className;
- (BOOL)updateToCurrentTimeByClass:(NSString *)className andStage:(NSString *)stage;
- (BOOL)updateAllToNoTime;

@end
