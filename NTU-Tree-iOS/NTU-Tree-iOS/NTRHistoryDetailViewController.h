//
//  NTRHistoryDetailViewController.h
//  NTU-Tree-iOS
//
//  Created by Huang Yi on 12/12/11.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark HistoryData

@interface NTRHistoryData : NSObject

@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) UIImage *image;

- (id)initWithDate:(NSDate *)date andImage:(UIImage *)image;

@end

#pragma mark -
#pragma mark HistoryTableViewCell

@interface NTRHistoryTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UIImageView *petLabel;

@end

#pragma mark -
#pragma mark HistoryDetailViewController

@interface NTRHistoryDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    NSString *petClass;
    
    UIView *footerView;
}

@property (strong, nonatomic) NSArray *data;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

- (id)initWithClass:(NSString *)className;
- (IBAction)handleTap:(id)sender;

@end
