//
//  NTRPageViewController.m
//  NTU-Tree-iOS
//
//  Created by Huang Yi on 12/11/22.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "NTRPageController.h"
#import "NTRTreeDetailViewController.h"
#import "NTRTabBarViewController.h"
#import "NTRUtility.h"

#import "NTRDataSource.h"
#import "NTRPetInformation.h"

#pragma mark PageTableViewController

static NSString *cellIdentifier = @"SimpleCell";

@interface NTRPageTableViewController ()

@property (strong, nonatomic, readwrite) NSString *areaName;
@property (strong, nonatomic, readwrite) NSArray *trees;

@property (weak, nonatomic, readwrite) UIViewController *superviewController;

@end

@implementation NTRPageTableViewController

- (id)initWithTitle:(NSString *)title contents:(NSArray *)contents andSuperViewController:(UIViewController *)controller {
    if(self = [super initWithNibName:@"NTRPageTableViewController" bundle:nil]) {
        _areaName = title;
        _trees = contents;
        
        _superviewController = controller;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView.layer setCornerRadius:10.0f];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellIdentifier];
}

#pragma mark DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.trees count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static UIView *disabledBackgroundView = nil;
    if(disabledBackgroundView == nil) {
        disabledBackgroundView = [[UIView alloc] init];
        disabledBackgroundView.backgroundColor = [UIColor colorWithRed:0.5f green:0.5f blue:0.5f alpha:0.5f];
    }
    
    static UIView *selectedBackgroundView = nil;
    if(selectedBackgroundView == nil) {
        selectedBackgroundView = [[UIView alloc] init];
        selectedBackgroundView.backgroundColor = [UIColor colorWithRed:0.52f green:0.26f blue:0.0f alpha:0.5f];
    }
    
    NSDictionary *tree = self.trees[indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.selectedBackgroundView = selectedBackgroundView;
    cell.textLabel.text = tree[NTRDataKeyName];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    if([NTRTrue isEqualToString:[tree objectForKey:NTRDataKeyScanned]]) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.userInteractionEnabled = YES;
        cell.textLabel.enabled = YES;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.userInteractionEnabled = NO;
        cell.textLabel.enabled = NO;
    }

    return cell;
}

#pragma mark Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *tree = self.trees[indexPath.row];
    NTRTreeDetailViewController *controller = [[NTRTreeDetailViewController alloc] initWithName:tree[NTRDataKeyName] andRegion:self.areaName];
    
    [self.superviewController.navigationController pushViewController:controller animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *tree = self.trees[indexPath.row];
    return [NTRTrue isEqualToString:[tree objectForKey:NTRDataKeyScanned]];
}

#pragma mark Others

- (void)unhighlightSelectedCell {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    if(indexPath != nil) {
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

}

@end

#pragma mark -
#pragma mark PageController

@interface NTRPageController ()

@property (strong, nonatomic, readwrite) UILabel *rightBarButton;
@property (strong, nonatomic, readwrite) NSMutableArray *tableViewControllers;

@end

@implementation NTRPageController

-(id)initWithCoder:(NSCoder *)aDecoder {
    if(self = [super initWithCoder:aDecoder]) {
        storyController = nil;
        
        titles = [[NTRDataSource sharedDataSource] arrayWithRegions];
        pageCount = [titles count];
        
        // for qrcode parameter passing
        _treeId = nil;
        
        _rightBarButton = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f, 21.0f)];
        _tableViewControllers = [[NSMutableArray alloc] initWithCapacity:pageCount];
        for(unsigned int i = 0; i < pageCount; i++) {
            [_tableViewControllers addObject:[NSNull null]];
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundTreeList.png" withSize:self.view.frame.size]];
    
    [self.rightBarButton setBackgroundColor:[UIColor clearColor]];
    [self.rightBarButton setFont:[UIFont fontWithName:@"Helvetica" size:18.0f]];
    [self.rightBarButton setTextColor:[UIColor whiteColor]];
    [self.rightBarButton setShadowColor:[UIColor darkGrayColor]];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:self.rightBarButton]];
    
    CGRect frame = self.scrollView.frame;
    frame.size.width = self.view.frame.size.width;
    frame.size.height = self.view.frame.size.height - self.navigationController.navigationBar.frame.size.height - self.tabBarController.tabBar.frame.size.height;
    self.scrollView.frame = frame;
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * pageCount, self.scrollView.frame.size.height);
    
    self.scrollView.scrollsToTop = NO;
    self.scrollView.delegate = self;
    
    self.pageControl.numberOfPages = pageCount;
    self.pageControl.currentPage = 0;
    
    [self.pageControl addTarget:self action:@selector(pageAction:) forControlEvents:UIControlEventValueChanged];
    
    [self setScrollViewWithPageNumber:self.pageControl.currentPage];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    scannedTreesCount = 0, totalTreesCount = 0;
    
    NTRDataSource *dataSource = [NTRDataSource sharedDataSource];
    for(NSString *title in titles) {
        scannedTreesCount += [dataSource countOfScannedTreesInRegion:title];
        totalTreesCount += [dataSource countOfTreesInRegion:title];
    }
    
    [self.rightBarButton setText:[NSString stringWithFormat:@"%.0f%%", floor((double)scannedTreesCount * 100.0f / (double)totalTreesCount)]];
    [self.rightBarButton sizeToFit];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NTRPageTableViewController *controller = [self.tableViewControllers objectAtIndex:self.pageControl.currentPage];
    if((NSNull *)controller != [NSNull null]) {
        [controller unhighlightSelectedCell];
    }
    
    if(self.treeId != nil) {
        [self selectTreeWithId:self.treeId];
        self.treeId = nil;
    }
    else {
        if(scannedTreesCount == 0 && storyController == nil) {
            storyController = [[NTRFullPagePromptController alloc] initWithText:@"為了獎勵你答應國王要養出許多小樹，守護世界的安危，皇后送了一棵大王椰子讓你餵食小樹。餵食小樹也可以讓圖鑑更豐富唷！\n\n趕快去看看圖鑑多了什麼！"];
            [storyController setBackgroundImage:[UIImage imageNamed:@"backgroundRainbow.png"]];
            [storyController setDelegate:self];
            [storyController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
            
            [storyController.button setTitle:@"現在去看" forState:UIControlStateNormal];
            
            [self presentViewController:storyController animated:YES completion:nil];
        }
    }
}

#pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if(isPageControlUsed) {
        return;
    }
    
    CGFloat pageWidth = self.scrollView.frame.size.width;
    self.pageControl.currentPage = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    [self setScrollViewWithPageNumber:self.pageControl.currentPage];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    isPageControlUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    isPageControlUsed = NO;
}

- (void)pageAction:(UIPageControl *)pageControl {
    [self pageAction:pageControl animated:YES];
}

- (void)pageAction:(UIPageControl *)pageControl animated:(BOOL)animated {
    int page = pageControl.currentPage;
    [self setScrollViewWithPageNumber:page];
    
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    
    [self.scrollView scrollRectToVisible:frame animated:animated];
    isPageControlUsed = YES;
}

- (void)setScrollViewWithPageNumber:(NSInteger)page {
    [self loadScrollViewWithPageNumber:page];
    [self loadScrollViewWithPageNumber:page + 1];
    [self loadScrollViewWithPageNumber:page - 1];
    
    NSString *title = titles[page];
    [self.navigationItem setTitle:title];
}

- (void)loadScrollViewWithPageNumber:(NSInteger)page {
    if(page < 0 || page >= pageCount) {
        return;
    }
    
    NTRPageTableViewController *tableViewController = [self.tableViewControllers objectAtIndex:page];
    
    if((NSNull *)tableViewController == [NSNull null]) {
        NSString *title = titles[page];
        NSArray *contents = [[NTRDataSource sharedDataSource] arrayWithGroupedTreesInRegion:title];
        
        tableViewController =  [[NTRPageTableViewController alloc] initWithTitle:title contents:contents andSuperViewController:self];
        
        if(tableViewController != nil) {
            [self.tableViewControllers replaceObjectAtIndex:page withObject:tableViewController];
        }
    }
    
    if(tableViewController.view.superview == nil) {
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        
        tableViewController.view.frame = frame;
        [self.scrollView addSubview:tableViewController.view];
    }
}

#pragma mark IBActions

- (IBAction)changePage:(id)sender {
    [self pageAction:self.pageControl];
}

#pragma mark NTRFullPagePromptDelegate

- (void)handleFullPagePromptButton:(id)sender {
    [sender dismissViewControllerAnimated:YES completion:^{
        [self selectTreeWithId:@"M+0-0_0719"];
        storyController = nil;
    }];
}

- (void)handleFullPagePromptDismiss:(id)sender {
    // do nothing
}

#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(buttonIndex > 0) {
        [self.tabBarController setSelectedIndex:1];
    }
}

#pragma mark Others

- (void)selectTreeWithId:(NSString *)treeId {
    NTRDataSource *dataSource = [NTRDataSource sharedDataSource];
    NSString *petName = [NTRPetInformation name];
    NSString *className = [NTRPetInformation className];
    
    NSDictionary *treeData = [dataSource treeByID:treeId];
    
    if(treeData != nil) {
        BOOL scanned = [NTRTrue isEqualToString:[treeData objectForKey:NTRDataKeyScanned]];
        
        if(!scanned) {
            // new tree, update database
            [dataSource updateToScannedByTreeId:treeId];
            
            treeData = [dataSource treeByID:treeId];
        }
        
        NSString *region = [treeData objectForKey:NTRDataKeyRegion];
        NSString *name = [treeData objectForKey:NTRDataKeyName];
        
        int page = [dataSource indexOfRegion:region];
        NTRPageTableViewController *tableViewController = [self.tableViewControllers objectAtIndex:page];
        
        if((NSNull *)tableViewController == [NSNull null]) {
            // load the controller with new data
            [self loadScrollViewWithPageNumber:page];
            tableViewController = [self.tableViewControllers objectAtIndex:page];
        }
        else if(!scanned) {
            // update the controller with new data
            tableViewController.trees = [dataSource arrayWithGroupedTreesInRegion:region];
            [tableViewController.tableView reloadData];
        }
        
        BOOL newRegion = [dataSource countOfScannedTreesInRegion:region] == 1;
        
        // update experience point
        BOOL hasLeveledUp;
        if(scanned) {
            hasLeveledUp = [NTRPetInformation increaseExperienceWithGain:NTRExperienceSmallGain];
        }
        else if(!newRegion) {
            hasLeveledUp = [NTRPetInformation increaseExperienceWithGain:NTRExperienceMedianGain];
        }
        else {
            hasLeveledUp = [NTRPetInformation increaseExperienceWithGain:NTRExperienceSuperGain];
        }
        
        self.pageControl.currentPage = page;
        [self pageAction:self.pageControl animated:NO];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[dataSource indexOfTree:name inRegion:region] inSection:0];
        [tableViewController tableView:tableViewController.tableView didSelectRowAtIndexPath:indexPath];
        
        NTRTabBarViewController *tabBarController = (NTRTabBarViewController *)self.tabBarController;
        [tabBarController handleLevelUp:hasLeveledUp forPet:petName andPetClass:className withDelegate:self];
    }
}

@end
