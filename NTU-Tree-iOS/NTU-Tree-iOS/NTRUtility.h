//
//  NTRUtility.h
//  NTU-Tree-iOS
//
//  Created by Huang Yi on 12/12/12.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark UIImage Resize

@interface UIImage (NTRResize)

+ (UIImage *)imageNamed:(NSString *)name withSize:(CGSize)size;
- (UIImage *)resize:(CGSize)newSize;

@end

#pragma mark -
#pragma mark UIView Animation

@interface UIView (NTRAnimation)

- (void)fadeInWithDuration:(NSTimeInterval)duration;
- (void)fadeInWithDuration:(NSTimeInterval)duration completion:(void (^)(BOOL finished))completion;
- (void)fadeOutWithDuration:(NSTimeInterval)duration;
- (void)fadeOutWithDuration:(NSTimeInterval)duration completion:(void (^)(BOOL finished))completion;
- (void)fadeTo:(CGFloat)alpha withDuration:(NSTimeInterval)duration completion:(void (^)(BOOL finished))completion;

@end

#pragma mark -
#pragma mark NSURL Parse

@interface NSURL (NTRParse)

- (NSString *)valueWithKey:(NSString *)key;

@end
