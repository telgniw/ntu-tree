//
//  NTRAppDelegate.h
//  NTU-Tree-iOS
//
//  Created by Huang Yi on 12/11/16.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import "ZBarSDK.h"
#import <UIKit/UIKit.h>

@interface NTRAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
