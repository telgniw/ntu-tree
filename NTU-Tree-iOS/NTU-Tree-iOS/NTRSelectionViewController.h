//
//  NTRInitialViewController.h
//  NTU-Tree-iOS
//
//  Created by Huang Yi on 12/12/18.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NTRSelectionViewDelegate <NSObject>

- (void)handleSelection:(id)sender;

@end

@interface NTRSelectionViewController : UIViewController <UIAlertViewDelegate> {
    NSString *className;
}

@property (nonatomic) id<NTRSelectionViewDelegate> delegate;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapLeft;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapRight;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewLeft;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewRight;
@property (strong, nonatomic) IBOutlet UILabel *nameLabelLeft;
@property (strong, nonatomic) IBOutlet UILabel *nameLabelRight;
@property (strong, nonatomic) IBOutlet UITextView *tipsView;

- (IBAction)handleTap:(id)sender;

@end
