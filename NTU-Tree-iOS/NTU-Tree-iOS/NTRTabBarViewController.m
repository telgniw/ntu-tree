//
//  NTRTabBarViewController.m
//  NTU-Tree-iOS
//
//  Created by Huang Yi on 12/11/22.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import "NTRTabBarViewController.h"
#import "NTRMainViewController.h"
#import "NTRHistoryPageViewController.h"
#import "NTRPageController.h"

@interface NTRTabBarViewController ()

@end

@implementation NTRTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // set initial tab to our small pet-tree
    self.selectedIndex = 1;
    
    // set customized background
    [self.tabBar setBackgroundColor:[UIColor colorWithRed:0.52f green:0.26f blue:0.0f alpha:1.0f]];
    [self.tabBar setTintColor:[UIColor brownColor]];
    
    [self.tabBar setSelectionIndicatorImage:[UIImage imageNamed:@"TabBarSelected-iPhone.png"]];
    [self.tabBar setSelectedImageTintColor:[UIColor yellowColor]];
    
    // set customized style for each tab-bar item
    for(id item in self.tabBar.items) {
        if([item isKindOfClass:[UITabBarItem class]]) {
            [item setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], UITextAttributeTextColor, [UIFont systemFontOfSize:12.0f], UITextAttributeFont, nil] forState:UIControlStateNormal];
            [item setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], UITextAttributeTextColor, [UIFont boldSystemFontOfSize:12.0f], UITextAttributeFont, nil] forState:UIControlStateSelected];
        }
    }
}

- (void)updateWithTreeId:(NSString *)treeId {
    // switch to tree tab
    [self setSelectedIndex:2];
    UINavigationController *controller = (UINavigationController *)self.childViewControllers[2];
    
    // pop to root view controller before navigate on its page controller
    [controller popToRootViewControllerAnimated:NO];
    
    // get the root view controller in the navigation controller
    NTRPageController *pageController = (NTRPageController *)controller.viewControllers[0];
    pageController.treeId = treeId;
}

- (void)handleLevelUp:(NTRLevelUpStatus)hasLeveledUp forPet:(NSString *)petName andPetClass:(NSString *)className withDelegate:(id<UIAlertViewDelegate>)delegate {
    // set update-needed for pet tab and history tab
    NTRMainViewController *mainController = (NTRMainViewController *)self.childViewControllers[1];
    [mainController setUpdateNeeded];
    
    UINavigationController *controller = (UINavigationController *)self.childViewControllers[3];
    [controller popToRootViewControllerAnimated:NO];
    
    NTRHistoryPageViewController *historyController = controller.viewControllers[0];
    [historyController reload];
    
    if(hasLeveledUp == NTRLevelUpMiddle) {
        [[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@升級了", petName]
                                    message:[NSString stringWithFormat:@"耶！你的%@升級了唷，快去看看它吧！", petName]
                                   delegate:delegate
                          cancelButtonTitle:@"再等等"
                          otherButtonTitles:@"馬上去", nil] show];
    }
    else if(hasLeveledUp == NTRLevelUpMax) {
        controller = (UINavigationController *)self.childViewControllers[3];
        
        NTRHistoryPageViewController *historyController = (NTRHistoryPageViewController *)controller.viewControllers[0];
        [historyController setPetName:petName];
        [historyController setPetClass:className];
        [historyController reload];
        
        [self setSelectedIndex:3];
    }
}

- (void)resetAll {
    UINavigationController *controller = (UINavigationController *)self.childViewControllers[2];
    [controller popToRootViewControllerAnimated:NO];
    
    controller = (UINavigationController *)self.childViewControllers[3];
    [controller popToRootViewControllerAnimated:NO];

    NTRHistoryPageViewController *historyController = controller.viewControllers[0];
    [historyController reload];
}

@end
