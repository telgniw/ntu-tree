//
//  NTRHistoryPageViewController.h
//  WoodFairy
//
//  Created by Huang Yi on 12/12/27.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import "NTRPageViewController.h"
#import "NTRFullPagePromptController.h"
#import "NTRStoryEndingViewController.h"

@interface NTRHistoryPageViewController : NTRPageViewController <NTRPageViewDelegate, NTRFullPagePromptDelegate> {
    NTRStoryEndingViewController *storyController;
}

@property (strong, nonatomic) NSString *petName;
@property (strong, nonatomic) NSString *petClass;

@end
