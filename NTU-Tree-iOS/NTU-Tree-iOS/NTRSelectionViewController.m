//
//  NTRInitialViewController.m
//  NTU-Tree-iOS
//
//  Created by Huang Yi on 12/12/18.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "NTRSelectionViewController.h"
#import "NTRMainViewController.h"
#import "NTRUtility.h"

#import "NTRDataSource.h"
#import "NTRPetInformation.h"

static NSString * const classNameLeft   = @"椰子";
static NSString * const classNameRight  = @"楓香";

@interface NTRSelectionViewController ()

@end

@implementation NTRSelectionViewController

- (id)init {
    if(self = [super initWithNibName:@"NTRSelectionViewController" bundle:nil]) {
        className = nil;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundMain.png" withSize:self.view.frame.size]];
    
    [self.tipsView setFont:[UIFont fontWithName:@"DFGirlStd-W5" size:32.0]];
    [self.nameLabelLeft setFont:[UIFont fontWithName:@"DFGirlStd-W5" size:17.0]];
    [self.nameLabelRight setFont:[UIFont fontWithName:@"DFGirlStd-W5" size:17.0]];
}

#pragma mark -
#pragma mark IBAction

- (IBAction)handleTap:(id)sender {
    if(sender == self.tapLeft) {
        className = classNameLeft;
    }
    else if(sender == self.tapRight) {
        className = classNameRight;
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"為它取個名字"
                                                        message:nil
                                                       delegate:self
                                              cancelButtonTitle:@"取消"
                                              otherButtonTitles:@"完成", nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alertView show];
}

#pragma mark -
#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(buttonIndex > 0) {
        [NTRPetInformation initWithNewName:[alertView textFieldAtIndex:0].text forClass:className];
        [self.delegate handleSelection:self];
    }
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView {
    return [alertView textFieldAtIndex:0].text.length > 0;
}

@end
