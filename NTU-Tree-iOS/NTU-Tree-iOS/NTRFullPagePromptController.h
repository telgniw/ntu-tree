//
//  NTRFullPagePromptController.h
//  WoodFairy
//
//  Created by Huang Yi on 12/12/26.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NTRFullPagePromptDelegate <NSObject>

- (void)handleFullPagePromptDismiss:(id)sender;
- (void)handleFullPagePromptButton:(id)sender;

@end

@interface NTRFullPagePromptController : UIViewController {
    NSString *animationText;
    
    NSInteger animationProgress;
    NSInteger delayProgress;
    NSInteger delay;
    
    CGRect maximumFrame;
}

@property (strong, nonatomic) NSTimer *timer;

@property (nonatomic) id<NTRFullPagePromptDelegate> delegate;
@property (nonatomic) BOOL shouldDismissAfterRestart;

@property (strong, nonatomic) IBOutlet UILabel *textLabel;
@property (strong, nonatomic) IBOutlet UIButton *button;

- (id)initWithText:(NSString *)text;
- (id)initWithNibName:(NSString *)nibNameOrNil andText:(NSString *)text;

- (void)setBackgroundImage:(UIImage *)image;

- (IBAction)handleButton:(id)sender;

@end
