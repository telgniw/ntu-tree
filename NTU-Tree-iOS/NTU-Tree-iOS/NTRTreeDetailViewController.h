//
//  NTRTreeDetailViewController.h
//  NTU-Tree-iOS
//
//  Created by Huang Yi on 12/12/11.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NTRTreeDetailViewController : UIViewController

@property (strong, nonatomic, readonly) NSString *name;
@property (strong, nonatomic, readonly) NSString *region;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *binomialLabel;
@property (strong, nonatomic) IBOutlet UILabel *classLabel;
@property (strong, nonatomic) IBOutlet UILabel *storyLabel;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

- (id)initWithName:(NSString *)name;
- (id)initWithName:(NSString *)name andRegion:(NSString *)region;

@end
