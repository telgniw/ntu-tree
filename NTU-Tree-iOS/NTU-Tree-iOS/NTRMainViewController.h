//
//  NTRMainViewController.h
//  NTU-Tree-iOS
//
//  Created by Huang Yi on 12/12/9.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NTRSelectionViewController.h"
#import "NTRFullPagePromptController.h"

@interface NTRMainViewController : UIViewController<UIAlertViewDelegate, NTRFullPagePromptDelegate, NTRSelectionViewDelegate> {
    NTRFullPagePromptController *storyController;
    
    BOOL updateNeeded;
    int dialogSeq;
    
    int pokeCount;
    int previousMessage;
    NSArray *messages;
}

@property (strong, nonatomic) IBOutlet UILabel *dialogLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *progressLabel;
@property (strong, nonatomic) IBOutlet UIProgressView *progressView;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapView;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *doubleTapView;
@property (strong, nonatomic) IBOutlet UIImageView *dialogView;

- (void)setUpdateNeeded;

@end
