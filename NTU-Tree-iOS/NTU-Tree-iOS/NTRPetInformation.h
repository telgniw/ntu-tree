//
//  NTRPetInfo.h
//  NTU-Tree-iOS
//
//  Created by Huang Yi on 12/12/16.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import <Foundation/Foundation.h>

extern const NSInteger NTRMaxExperience;

enum {
    NTRMoodNone         = 0,
    NTRMoodHappy        = 1,
    NTRMoodUnhappy      = 2,
};

typedef NSUInteger NTRPetMood;

enum {
    NTRExperienceSmallGain  = 0,
    NTRExperienceMedianGain = 1,
    NTRExperienceSuperGain  = 2,
    NTRExperienceDemoGain   = 3,
};

typedef NSUInteger NTRPetExperienceGain;

enum {
    NTRLevelUpNone      = 0,
    NTRLevelUpMiddle    = 1,
    NTRLevelUpMax       = 2,
};

typedef NSUInteger NTRLevelUpStatus;

@interface NTRPetInformation : NSObject

+ (BOOL)firstUsed;
+ (NSString *)className;
+ (NSString *)imageName;
+ (NSString *)name;
+ (NSString *)stage;
+ (NTRPetMood)mood;
+ (NSInteger)experience;

+ (void)initWithNewName:(NSString *)name forClass:(NSString *)className;
+ (void)resetAll;
+ (NTRLevelUpStatus)increaseExperienceWithGain:(NTRPetExperienceGain)gain;

@end
