//
//  NTRDataSource.m
//  NTU-Tree-iOS
//
//  Created by Chun-Ta, Lin on 12/12/14.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import "NTRDataSource.h"

// Constants
NSString * const NTRTrue = @"True";
NSString * const NTRFalse = @"False";

// Cache Keys
static NSString *NTRCacheKeyTrees = @"NTRDataSource.Cache.%@.Trees";
static NSString *NTRCacheKeyRegions = @"NTRDataSource.Cache.Regions";

// Dictionary Keys
NSString * const NTRDataKeyID = @"ID";
NSString * const NTRDataKeyName = @"Name";
NSString * const NTRDataKeyRegion = @"Region";
NSString * const NTRDataKeyBinomial = @"Binomial";
NSString * const NTRDataKeyClass = @"Class";
NSString * const NTRDataKeyStory = @"Story";
NSString * const NTRDataKeyImage = @"Image";
NSString * const NTRDataKeyScanned = @"Scanned";
NSString * const NTRDataKeyTime = @"Time";
NSString * const NTRDataKeyStage = @"Stage";
NSString * const NTRDataKeyRecord = @"Record";

@interface NTRDataSource ()

@end

@implementation NTRDataSource

#pragma mark -
#pragma mark Object Lifecycle

+ (NTRDataSource *)sharedDataSource {
    static dispatch_once_t once;
    static NTRDataSource *sharedDataSource;
    dispatch_once(&once, ^ {
        sharedDataSource = [[self alloc] init];
    });
    return sharedDataSource;
}

- (id)init {
    if (self = [super init]) {
        NSString *destPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        treePath = [destPath stringByAppendingPathComponent:@"trees.plist"];
        historyPath = [destPath stringByAppendingPathComponent:@"history.plist"];
        
        // If the file doesn't exist in the Documents Folder, copy it.
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *sourcePath;
        
        if (![fileManager fileExistsAtPath:treePath]) {
            sourcePath = [[NSBundle mainBundle] pathForResource:@"trees" ofType:@"plist"];
            [fileManager copyItemAtPath:sourcePath toPath:treePath error:nil];
        }
        if (![fileManager fileExistsAtPath:historyPath]) {
            sourcePath = [[NSBundle mainBundle] pathForResource:@"history" ofType:@"plist"];
            [fileManager copyItemAtPath:sourcePath toPath:historyPath error:nil];
        }
        
        // Load the Property List.
        treeList = [NSArray arrayWithContentsOfFile:treePath];
        historyList = [NSArray arrayWithContentsOfFile:historyPath];
        
        cache = [[NSCache alloc] init];
    }
    
    return self;
}

#pragma mark -
#pragma mark Interfaces

- (void)refresh {
    treeList = [NSArray arrayWithContentsOfFile:treePath];
    historyList = [NSArray arrayWithContentsOfFile:historyPath];
    
    [self cleanCache];
}

- (void)cleanCache {
    [cache removeAllObjects];
}

#pragma mark -
#pragma mark trees.plist

- (NSDictionary *)treeByID:(NSString *)treeId {
    for(NSDictionary *tree in treeList) {
        if([treeId isEqualToString:tree[NTRDataKeyID]]) {
            return tree;
        }
    }
    return nil;
}

- (NSDictionary *)treeByName:(NSString *)name {
    for(NSDictionary *tree in treeList) {
        if([name isEqualToString:tree[NTRDataKeyName]]) {
            return tree;
        }
    }
    return nil;
}

- (NSDictionary *)treeByName:(NSString *)name andRegion:(NSString *)region {
    NSArray *trees = [self arrayWithTreesInRegion:region];
    for(NSDictionary *tree in trees) {
        if([name isEqualToString:tree[NTRDataKeyName]]) {
            return tree;
        }
    }
    return nil;
}


- (NSArray *)arrayWithRegions {
    NSMutableArray *regions = [cache objectForKey:NTRCacheKeyRegions];
    
    if(!regions) {
        // Save regions into a set.
        regions = [NSMutableArray array];
        NSMutableSet *regionSet = [NSMutableSet set];
        NSMutableArray *mutableRegions = [NSMutableArray array];
        for(NSDictionary *region in treeList) {
            NSString *name = region[NTRDataKeyRegion];
            if([regionSet containsObject:name]) {
                continue;
            }
            [regionSet addObject:name];
            [mutableRegions addObject:name];
        }
        regions = [NSArray arrayWithArray:mutableRegions];
        
        // Save the result into cache
        [cache setObject:regions forKey:NTRCacheKeyRegions];
    }
    
    return regions;
}

- (NSArray *)arrayWithTreesInRegion:(NSString *)region {
    NSString *cacheKey = [NSString stringWithFormat:NTRCacheKeyTrees, region];
    NSMutableArray *trees = [cache objectForKey:cacheKey];
    
    if(!trees) {
        // Save trees into a set.
        trees = [NSMutableArray array];
        for(NSDictionary *tree in treeList) {
            if([region isEqualToString:tree[NTRDataKeyRegion]]) {
                [trees addObject:tree];
            }
        }
        
        // Save the result into cache
        [cache setObject:trees forKey:cacheKey];
    }
    
    return trees;
}

- (NSArray *)arrayWithGroupedTreesInRegion:(NSString *)region {
    // Group trees according to name.
    NSArray *trees = [self arrayWithTreesInRegion:region];
    
    NSMutableArray *groupedTrees = [NSMutableArray array];
    NSMutableDictionary *nameIndexPairs = [NSMutableDictionary dictionary];
    for(NSDictionary *tree in trees) {
        NSString *name = tree[NTRDataKeyName];
        NSNumber *index = [nameIndexPairs objectForKey:name];
        
        NSMutableDictionary *groupedTree = nil;
        if(index != nil) {
            groupedTree = [groupedTrees[[index integerValue]] mutableCopy];
        }
        else {
            groupedTree = [NSMutableDictionary dictionary];
            groupedTree[NTRDataKeyName] = name;
            [nameIndexPairs setObject:[NSNumber numberWithInteger:[groupedTrees count]] forKey:name];
        }
        
        if([tree[NTRDataKeyScanned] isEqualToString:NTRTrue]) {
            groupedTree[NTRDataKeyScanned] = NTRTrue;
        }
        
        if(index != nil) {
            [groupedTrees replaceObjectAtIndex:[index integerValue] withObject:[NSDictionary dictionaryWithDictionary:groupedTree]];
        }
        else {
            [groupedTrees addObject:[NSDictionary dictionaryWithDictionary:groupedTree]];
        }
    }
    
    return [NSArray arrayWithArray:groupedTrees];
}

- (NSInteger)countOfTreesInRegion:(NSString *)region {
    return [[self arrayWithGroupedTreesInRegion:region] count];
}

- (NSInteger)countOfScannedTreesInRegion:(NSString *)region {
    NSInteger count = 0;
    NSArray *trees = [self arrayWithGroupedTreesInRegion:region];
    for(NSDictionary *tree in trees) {
        if([tree[NTRDataKeyScanned] isEqualToString:NTRTrue]) {
            count += 1;
        }
    }
    return count;
}

- (BOOL)updateToScannedByTreeId:(NSString *)treeId {
    return [self updateToScanned:YES byTreeId:treeId];
}

- (BOOL)updateToScanned:(BOOL)scanned byTreeId:(NSString *)treeId {
    for(int i = 0; i < [treeList count]; i++) {
        NSDictionary *tree = treeList[i];
        if([treeId isEqualToString:tree[NTRDataKeyID]]){
            // update scanned
            NSMutableArray *mutableTreeList = [treeList mutableCopy];
            NSMutableDictionary *mutableTree = [tree mutableCopy];
            NSString *value = (scanned == YES)? NTRTrue : NTRFalse;
            [mutableTree setObject:value forKey:NTRDataKeyScanned];
            [mutableTreeList replaceObjectAtIndex:i withObject:mutableTree];
            
            // write to plist
            BOOL status = [mutableTreeList writeToFile:treePath atomically:YES];
            if(status) {
                [self refresh];
            }
            else {
                NSLog(@"Warning: failed writing to file `%@`.", treePath);
            }
            
            return status;
        }
    }
    
    return NO;
}

- (BOOL)updateAllToScanned:(BOOL)scanned {
    NSMutableArray *mutableTreeList = [treeList mutableCopy];
    for(int i = 0; i < [mutableTreeList count]; i++) {
        NSMutableDictionary *mutableTree = [mutableTreeList[i] mutableCopy];
        NSString *value = (scanned == YES)? NTRTrue : NTRFalse;
        [mutableTree setObject:value forKey:NTRDataKeyScanned];
        [mutableTreeList replaceObjectAtIndex:i withObject:mutableTree];
    }
    
    // write to plist
    BOOL status = [mutableTreeList writeToFile:treePath atomically:YES];
    if(status) {
        [self refresh];
    }
    else {
        NSLog(@"Warning: failed writing to file `%@`.", treePath);
    }
    
    return status;
}

- (NSInteger)indexOfRegion:(NSString *)region {
    NSArray *regions = [self arrayWithRegions];
    for(int idx = 0; idx < [regions count]; idx++) {
        if([region isEqualToString:regions[idx]]) {
            return idx;
        }
    }
    return -1;
}

- (NSInteger)indexOfTree:(NSString *)name inRegion:(NSString *)region {
    NSArray *trees = [self arrayWithGroupedTreesInRegion:region];
    for(int idx = 0; idx < [trees count]; idx++) {
        if([trees[idx][NTRDataKeyName] isEqualToString:name]) {
            return idx;
        }
    }
    return -1;
}

#pragma mark -
#pragma mark history.plist

- (NSDictionary *)historyRecordsForClass:(NSString *)className andStage:(NSString *)stage {
    NSDictionary *history = [self historyForClass:className];
    for(NSDictionary *record in history[NTRDataKeyRecord]) {
        if([stage isEqualToString:record[NTRDataKeyStage]]) {
            return record;
        }
    }
    return nil;
}

- (NSDictionary *)historyForClass:(NSString *)className {
    for(NSDictionary *history in historyList) {
        if([className isEqualToString:history[NTRDataKeyClass]]) {
            return history;
        }
    }
    return nil;
}

- (NSString *)imageNameForClass:(NSString *)className {
    NSString *topStage = [self lastStageForClass:className];
    if(topStage == nil) {
        topStage = [self firstStageForClass:className];
    }
    return [self imageNameForClass:className andStage:topStage];
}

- (NSString *)imageNameForClass:(NSString *)className andStage:(NSString *)stage {
    NSDictionary *history = [self historyForClass:className];
    return [NSString stringWithFormat:history[NTRDataKeyImage], stage];
}

- (NSTimeInterval)timeForClass:(NSString *)className andStage:(NSString *)stage {
    NSDictionary *record = [self historyRecordsForClass:className andStage:stage];
    return (NSTimeInterval)[record[NTRDataKeyTime] doubleValue];
}

- (NSString *)firstStageForClass:(NSString *)className {
    NSDictionary *history = [self historyForClass:className];
    NSDictionary *firstRecord = history[NTRDataKeyRecord][0];
    return firstRecord[NTRDataKeyStage];
}

- (NSString *)lastStageForClass:(NSString *)className {
    NSDictionary *history = [self historyForClass:className];
    NSDictionary *lastRecord = nil;
    for(NSDictionary *record in history[NTRDataKeyRecord]) {
        NSTimeInterval time = [record[NTRDataKeyTime] doubleValue];
        if(time < 0) {
            break;
        }
        lastRecord = record;
    }
    return lastRecord[NTRDataKeyStage];
}

- (NSString *)nextStageForClass:(NSString *)className afterStage:(NSString *)stage {
    NSDictionary *history = [self historyForClass:className];
    NSArray *records = history[NTRDataKeyRecord];
    for(int i = 1; i < [records count]; i++) {
        if([stage isEqualToString:(NSString *)records[i - 1][NTRDataKeyStage]]) {
            return records[i][NTRDataKeyStage];
        }
    }
    return nil;
}

- (BOOL)updateToCurrentTimeByClass:(NSString *)className {
    return [self updateTime:[[NSDate date] timeIntervalSinceReferenceDate] byClass:className andStage:[self firstStageForClass:className]];
}

- (BOOL)updateToCurrentTimeByClass:(NSString *)className andStage:(NSString *)stage {
    return [self updateTime:[[NSDate date] timeIntervalSinceReferenceDate] byClass:className andStage:stage];
}

- (BOOL)updateAllToNoTime {
    NSMutableArray *mutableHistoryList = [historyList mutableCopy];
    for(int i = 0; i < [mutableHistoryList count]; i++) {
        NSMutableDictionary *mutableHistory = [mutableHistoryList[i] mutableCopy];
        NSMutableArray *mutableRecordList = [mutableHistory[NTRDataKeyRecord] mutableCopy];
        for(int j = 0; j < [mutableRecordList count]; j++) {
            NSMutableDictionary *mutableRecord = [mutableRecordList[j] mutableCopy];
            [mutableRecord setValue:@"-1" forKey:NTRDataKeyTime];
            [mutableRecordList replaceObjectAtIndex:j withObject:mutableRecord];
        }
        [mutableHistory setValue:mutableRecordList forKey:NTRDataKeyRecord];
        [mutableHistoryList replaceObjectAtIndex:i withObject:mutableHistory];
    }
    
    // write to plist
    BOOL status = [mutableHistoryList writeToFile:historyPath atomically:YES];
    if(status) {
        [self refresh];
    }
    else {
        NSLog(@"Warning: failed writing to file `%@`.", historyPath);
    }
    return status;
}

- (BOOL)updateTime:(NSTimeInterval)time byClass:(NSString *)className andStage:(NSString *)stage {
    for(int i = 0; i < [historyList count]; i++) {
        NSDictionary *history = historyList[i];
        if([className isEqualToString:history[NTRDataKeyClass]]) {
            NSArray *recordList = history[NTRDataKeyRecord];
            for(int j = 0; j < [recordList count]; j++) {
                NSDictionary *record = recordList[j];
                if([stage isEqualToString:record[NTRDataKeyStage]]) {
                    // update only when raising the pet for the first time
                    NSTimeInterval recordTime = [record[NTRDataKeyTime] doubleValue];
                    if(recordTime >= 0) {
                        return YES;
                    }
                    
                    NSMutableArray *mutableHistoryList = [historyList mutableCopy];
                    NSMutableDictionary *mutableHistory = [history mutableCopy];
                    NSMutableArray *mutableRecordList = [recordList mutableCopy];
                    NSMutableDictionary *mutableRecord = [record mutableCopy];
                    [mutableRecord setValue:[NSString stringWithFormat:@"%.0f", time] forKey:NTRDataKeyTime];
                    [mutableRecordList replaceObjectAtIndex:j withObject:mutableRecord];
                    [mutableHistory setValue:mutableRecordList forKey:NTRDataKeyRecord];
                    [mutableHistoryList replaceObjectAtIndex:i withObject:mutableHistory];
                    
                    // write to plist
                    BOOL status = [mutableHistoryList writeToFile:historyPath atomically:YES];
                    if(status) {
                        [self refresh];
                    }
                    else {
                        NSLog(@"Warning: failed writing to file `%@`.", historyPath);
                    }
                    
                    return status;
                }
            }
        }
    }
    return NO;
}

@end
