//
//  NTRFullPagePromptController.m
//  WoodFairy
//
//  Created by Huang Yi on 12/12/26.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "NTRFullPagePromptController.h"
#import "NTRUtility.h"

#pragma mark FullPagePromptController

@interface NTRFullPagePromptController ()

@end

@implementation NTRFullPagePromptController

#pragma mark -
#pragma mark Object Lifecycle

- (id)initWithText:(NSString *)text {
    return [self initWithNibName:@"NTRFullPagePromptController" andText:text];
}

- (id)initWithNibName:(NSString *)nibNameOrNil andText:(NSString *)text {
    if(self = [super initWithNibName:nibNameOrNil bundle:nil]) {
        animationText = text;
        delay = 5;
        
        _delegate = nil;
        _shouldDismissAfterRestart = NO;
    }
    return self;
}

- (void)dealloc {
    [self.timer invalidate];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    maximumFrame = self.textLabel.frame;
    
    [self.textLabel setFont:[UIFont fontWithName:@"DFGirlStd-W5" size:self.textLabel.font.pointSize]];
    [self.button.titleLabel setFont:[UIFont fontWithName:@"DFGirlStd-W5" size:self.button.titleLabel.font.pointSize]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if(self.shouldDismissAfterRestart == YES) {
        [self.view setBackgroundColor:[UIColor clearColor]];
        [self.textLabel setHidden:YES];
        [self.button setHidden:YES];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if(self.shouldDismissAfterRestart == YES) {
        [self dismissViewControllerAnimated:NO completion:^{
            [self.delegate handleFullPagePromptDismiss:self];
        }];
    }
    else {
        animationProgress = 0;
        delayProgress = 0;
        
        self.timer = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
    }
}

#pragma mark -
#pragma mark Others

- (void)setBackgroundImage:(UIImage *)image {
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[image resize:self.view.frame.size]]];
}

- (IBAction)handleButton:(id)sender {
    if(sender == self.button) {
        [self.textLabel fadeOutWithDuration:1.0f completion:^(BOOL finished) {
            [self.delegate handleFullPagePromptButton:self];
        }];
    }
}

- (void)timerAction:(id)sender {
    if(sender == self.timer) {
        if(delayProgress < delay) {
            delayProgress += 1;
        }
        else {
            static NSRange range;
            range.length = 1;
            range.location = animationProgress;
            
            NSString *newText = [self.textLabel.text stringByAppendingString:[animationText substringWithRange:range]];
            CGRect newFrame = maximumFrame;
            newFrame.size = [newText sizeWithFont:self.textLabel.font constrainedToSize:maximumFrame.size];
            
            [self.textLabel setText:newText];
            [self.textLabel setFrame:newFrame];
            
            CATransition *transition = [CATransition animation];
            transition.duration = 1.0f;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
            transition.type = kCATransitionFade;
            
            [self.textLabel.layer addAnimation:transition forKey:nil];
            
            animationProgress += 1;
            if(animationProgress >= [animationText length]) {
                [self.timer invalidate];
            }
        }
    }
}

@end
