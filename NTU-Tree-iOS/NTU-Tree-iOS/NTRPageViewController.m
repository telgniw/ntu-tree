//
//  NTRPageViewController.m
//  WoodFairy
//
//  Created by Huang Yi on 12/12/26.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import "NTRPageViewController.h"

@interface NTRPageViewController ()

@end

@implementation NTRPageViewController

#pragma mark Object Lifecycle

- (id)initWithCoder:(NSCoder *)aDecoder andNumberOfPages:(NSInteger)numberOfPages {
    if(self = [super initWithCoder:aDecoder]) {
        pageControlUsed = NO;
        
        _delegate = nil;
        
        _controllers = [NSMutableArray array];
        for(int i = 0; i < numberOfPages; i++) {
            [_controllers addObject:[NSNull null]];
        }
    }
    return self;
}

#pragma mark -
#pragma mark Controller Views

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSInteger numberOfPages = [self.controllers count];
    
    [self.scrollView setDelegate:self];
    
    CGRect frame = self.scrollView.frame;
    frame.size.width = self.view.frame.size.width;
    frame.size.height = self.view.frame.size.height - self.navigationController.navigationBar.frame.size.height - self.tabBarController.tabBar.frame.size.height;
    [self.scrollView setFrame:frame];
    
    CGSize contentSize = CGSizeMake(self.scrollView.frame.size.width * numberOfPages, self.scrollView.frame.size.height);
    [self.scrollView setContentSize:contentSize];
    
    [self.pageControl setNumberOfPages:numberOfPages];
    [self setCurrentPage:0];
}

#pragma mark -
#pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if(pageControlUsed == YES) {
        return;
    }
    
    CGFloat pageWidth = self.scrollView.frame.size.width;
    [self setCurrentPage:floor((self.scrollView.contentOffset.x - pageWidth / 2.0f) / pageWidth) + 1];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageControlUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlUsed = NO;
}

#pragma mark -
#pragma mark IBAction

- (IBAction)changePage:(id)sender {
    int page = self.pageControl.currentPage;
    
    [self loadViewForPage:page];
    [self loadViewForPage:page + 1];
    [self loadViewForPage:page - 1];
    
    CGRect frame = self.scrollView.frame;
    frame.origin.x = self.scrollView.frame.size.width * page;
    frame.origin.y = 0.0f;
    
    [self.scrollView scrollRectToVisible:frame animated:YES];
    
    pageControlUsed = YES;
}

#pragma mark -
#pragma mark Others

- (void)setCurrentPage:(NSInteger)page {
    [self.pageControl setCurrentPage:page];
    
    [self loadViewForPage:page];
    [self loadViewForPage:page + 1];
    [self loadViewForPage:page - 1];
}

- (void)loadViewForPage:(NSInteger)page {
    if(page < 0 || page >= [self.controllers count]) {
        return;
    }
    
    UIViewController *controller = [self.controllers objectAtIndex:page];
    if((NSNull *)controller == [NSNull null]) {
        controller = [self.delegate viewControllerForPage:page];
        [self.controllers replaceObjectAtIndex:page withObject:controller];
    }
    
    if(controller.view.superview == nil) {
        CGRect frame = self.scrollView.frame;
        frame.origin.x = self.scrollView.frame.size.width * page;
        frame.origin.y = 0.0f;
        
        [controller.view setFrame:frame];
        [self.scrollView addSubview:controller.view];
    }
}

- (void)reload {
    NSInteger numberOfPages = [self.controllers count];
    for(int i = 0; i < numberOfPages; i++) {
        UIViewController *controller = (UIViewController *)self.controllers[i];
        if((NSNull *)controller == [NSNull null]) {
            continue;
        }
        [controller.view removeFromSuperview];
        
        [self.controllers replaceObjectAtIndex:i withObject:[NSNull null]];
    }
    
    [self setCurrentPage:0];
}

@end
