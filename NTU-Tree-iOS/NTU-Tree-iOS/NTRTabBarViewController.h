//
//  NTRTabBarViewController.h
//  NTU-Tree-iOS
//
//  Created by Huang Yi on 12/11/22.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NTRPetInformation.h"

@interface NTRTabBarViewController : UITabBarController

- (void)updateWithTreeId:(NSString *)treeId;
- (void)handleLevelUp:(NTRLevelUpStatus)hasLeveledUp forPet:(NSString *)petName andPetClass:(NSString *)className withDelegate:(id<UIAlertViewDelegate>)delegate;
- (void)resetAll;

@end
