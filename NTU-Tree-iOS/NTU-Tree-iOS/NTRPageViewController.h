//
//  NTRPageViewController.h
//  WoodFairy
//
//  Created by Huang Yi on 12/12/26.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NTRPageViewDelegate <NSObject>

- (UIViewController *)viewControllerForPage:(NSInteger)page;

@end

@interface NTRPageViewController : UIViewController <UIScrollViewDelegate> {
    BOOL pageControlUsed;
}

@property (nonatomic) id<NTRPageViewDelegate> delegate;

@property (strong, nonatomic) NSMutableArray *controllers;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;

- (id)initWithCoder:(NSCoder *)aDecoder andNumberOfPages:(NSInteger)numberOfPages;

- (void)reload;

- (IBAction)changePage:(id)sender;

@end
