//
//  NTRPageViewController.h
//  NTU-Tree-iOS
//
//  Created by Huang Yi on 12/11/22.
//  Copyright (c) 2012年 NTU Mobile HCI Lab. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NTRFullPagePromptController.h"

#pragma mark PageTableViewController

@interface NTRPageTableViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic, readonly) NSString *areaName;
@property (strong, nonatomic, readonly) NSArray *trees;

@property (weak, nonatomic, readonly) UIViewController *superviewController;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

- (id)initWithTitle:(NSString *)title contents:(NSArray *)contents andSuperViewController:(UIViewController *)controller;

@end

#pragma mark -
#pragma mark PageController

@interface NTRPageController : UIViewController<NTRFullPagePromptDelegate, UIScrollViewDelegate, UIAlertViewDelegate> {
    NTRFullPagePromptController *storyController;
    
    int pageCount, scannedTreesCount, totalTreesCount;
    BOOL isPageControlUsed;
    
    NSArray *titles;
}

@property (strong, nonatomic) NSString *treeId;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;

@property (strong, nonatomic, readonly) UILabel *rightBarButton;
@property (strong, nonatomic, readonly) NSMutableArray *tableViewControllers;

- (IBAction)changePage:(id)sender;

@end
