# Clone the Project #

Clone the whole project recursively including submodules.

    git clone --recursive git@bitbucket.org:telgniw/ntu-tree.git

The above is equivalent to using the following commands.

    git clone git@bitbucket.org:telgniw/ntu-tree.git
    git submodule update --init

See [here](http://stackoverflow.com/questions/3796927) for reference.

# 3rd-Party Libraries #

Update the 3rd-party libraries using the commnad `git submodule update`.
See [here](http://markhansen.co.nz/git-repo-inside-git/) for more information about `git submodule`.

# License #

Copyright (c) 2012 [Yi Huang], [Tzu-Chun Chen], [Peter Lin]

[Yi Huang]: http://bitbucket.org/telgniw
[Tzu-Chun Chen]: http://bitbucket.org/kelly79126
[Peter Lin]: http://bitbucket.org/silentvow
